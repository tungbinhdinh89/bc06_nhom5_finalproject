import React from "react";
import UserFooter from "./UserFooter/UserFooter";
import UserHeader from "./UserHeader/UserHeader";

function UserTemplate({ Component }) {
  return (
    <div>
      <UserHeader />
      <Component />
      <UserFooter />
    </div>
  );
}

export default UserTemplate;
