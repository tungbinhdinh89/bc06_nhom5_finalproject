import { SET_ORDER_ROOM } from "../constant/userConstant";

const initialState = {
  orderRoom: [],
};

const userOrderRoomReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ORDER_ROOM:
      //   state.userSignUp = action.userSignUp;
      state.orderRoom.push(action.payload);

      return { ...state };

    default:
      return state;
  }
};

export default userOrderRoomReducer;
