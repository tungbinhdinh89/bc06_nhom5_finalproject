import { localAdminService } from "../../Services/localServ";
import { ADMIN_LOGIN } from "../constant/userConstant";

const initialState = {
  userInfo: localAdminService.get(),
  userRegisterInfo: null,
  token: localAdminService.get()?.token || null,
};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADMIN_LOGIN:
      const { token, ...userInfo } = payload;
      localAdminService.set({ token });
      return {
        ...state,
        userInfo,
        token,
      };

    default:
      return state;
  }
};

export default userReducer;
