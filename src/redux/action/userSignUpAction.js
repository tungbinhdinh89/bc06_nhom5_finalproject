import { postSignUp } from "../../Services/userServ";

import { SET_USER_SIGNUP, USER_SIGNUP } from "../constant/userConstant";

// redux-thunk: gọi api trực tiếp trong action

export const setUserSignUpAction = (formData, onSuccess, onError) => {
  return (dispatch) => {
    postSignUp(formData)
      .then((res) => {
        console.log(res);

        dispatch({
          type: USER_SIGNUP,
          userSignUp: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
        if (
          err.response &&
          err.response.data &&
          err.response.data.content === "Email đã tồn tại !"
        ) {
          onError("Yêu cầu không hợp lệ, email đã tồn tại");
        }
      });
  };
};
export const setUserSignUp = (userData) => {
  return { type: SET_USER_SIGNUP, payload: userData };
};
