import React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { localUserServ } from "../../Services/localServ";

function SubMenu({ setOpen }) {
  const navigate = useNavigate();
  const refreshPage = () => {
    navigate(0);
  };
  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);

  const userLoginInfo = userSingIn?.user;

  let handleLogout = () => {
    localUserServ.remove();
    navigate("/");
    refreshPage();
  };

  return (
    // No Login
    userLoginInfo ? (
      <ul className="bg-white min-w-[250px] px-1 py-1 text-black shadow-lg rounded">
        <>
          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] font-bold rounded-md">
              Tin nhắn
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] font-bold rounded-md ">
              Chuyến đi
            </li>
          </Link>

          <Link to="/personal-info" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] font-bold rounded-md border-b-2">
              Thông tin cá nhân
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md">
              Cho thuê nhà
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md">
              Tổ chức trải nghiệm
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md border-b-2">
              Trợ giúp
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li
              className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md font-bold"
              onClick={handleLogout}>
              Đăng xuất
            </li>
          </Link>
        </>
      </ul>
    ) : (
      <ul className="bg-white  min-w-[250px] px-1 py-1 text-black shadow-lg rounded">
        <>
          <Link to="/sign-up">
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] font-bold rounded-md">
              Đăng Ký
            </li>
          </Link>

          <Link to="/sign-in">
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] font-bold rounded-md border-b-2">
              Đăng Nhập
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md">
              Cho thuê nhà
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md">
              Tổ chức trải nghiệm
            </li>
          </Link>

          <Link to="/" onClick={() => setOpen(false)}>
            <li className="h-12 flex justify-between items-center px-3 hover:bg-black/[0.03] rounded-md">
              Trợ giúp
            </li>
          </Link>
        </>
      </ul>
    )

    // Login
  );
}

export default SubMenu;
