import React from "react";
import { Link } from "react-router-dom";

const SideBarItem = ({ item, active }) => {
  const Icon = item.icon;
  return (
    <Link
      to={item.path}
      className={active ? "sidebar-item-active" : "sidebar-item"}
    >
      <span className="sidebar-item-icon">
        <Icon /> {/* Sử dụng biến icon như một component */}
      </span>
      <span className="sidebar-item-label">{item.title}</span>
    </Link>
  );
};
export default SideBarItem;
