import axios from "axios";
import { BASE_URL, configHeaders } from "../utils/apiURL";

export const getRoomList = () => {
  const response = axios.get(`${BASE_URL}/api/phong-thue`, {
    headers: configHeaders(),
  });

  return response;
};

export const getRoomDetail = (id) => {
  const response = axios.get(`${BASE_URL}/api/phong-thue/${id}`, {
    headers: configHeaders(),
  });

  return response;
};
