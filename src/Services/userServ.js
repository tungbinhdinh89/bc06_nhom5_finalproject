import axios from "axios";
// import api from "./apiClient";
import { BASE_URL, configHeaders, configUserToken } from "../utils/apiURL";

// get
// export const getComments = async (maPhong) => {
//   const response = await api.get(
//     `/api/binh-luan/lay-binh-luan-theo-phong/${maPhong}`
//   );

//   return response.data;
// };
// export const getComments = (maPhong) => {
//   const response = axios.get(
//     `${BASE_URL}/api/binh-luan/lay-binh-luan-theo-phong/${maPhong}`,
//     {
//       headers: configHeaders(),
//     }
//   );

//   return response.data;
// };

export const getComments = (maPhong) => {
  const response = axios.get(
    `${BASE_URL}/api/binh-luan/lay-binh-luan-theo-phong/${maPhong}`,
    {
      headers: configHeaders(),
    }
  );

  return response;
};

export const getOderRoomByUser = (maNguoiDung) => {
  const response = axios.get(
    `${BASE_URL}/api/dat-phong/lay-theo-nguoi-dung/${maNguoiDung}`,
    {
      headers: configHeaders(),
    }
  );

  return response;
};

//post

export const postComments = (formComment, userToken) => {
  const response = axios.post(`${BASE_URL}/api/binh-luan`, formComment, {
    headers: configHeaders(),
    userToken,
  });

  return response;
};

export const postSignUp = (formSignUp) => {
  const response = axios.post(`${BASE_URL}/api/auth/signup`, formSignUp, {
    headers: configHeaders(),
  });

  return response;
};

export const postSignIn = (formSignIn) => {
  const response = axios.post(`${BASE_URL}/api/auth/signin`, formSignIn, {
    headers: configHeaders(),
  });

  return response;
};

export const postUserAvatar = (formFile, userToken) => {
  console.log("formFile: ", formFile);
  const response = axios.post(`${BASE_URL}/api/users/upload-avatar`, formFile, {
    headers: configUserToken(),
    // token: userToken,
  });

  return response;
};

// put
export const putEditUserInfo = (formInfo, userID) => {
  const response = axios.put(`${BASE_URL}/api/users/${userID}`, formInfo, {
    headers: configHeaders(),
  });

  return response;
};
// admin
// export const userService = {
//   postLogin: (formLogin) => {
//     return axios({
//       url: `${BASE_URL}/api/auth/signin`,
//       method: "POST",
//       data: formLogin,
//       headers: configHeaders(),
//     });
//   },
// };

// export const postLogin = (formLogin) => {
//   const response = axios.post(`${BASE_URL}/api/auth/signin`, formLogin, {
//     headers: configHeaders(),
//   });

//   return response;
// };
