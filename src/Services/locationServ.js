import axios from 'axios';
import { BASE_URL, configHeaders } from '../utils/apiURL';

export const getLocationList = () => {
  const response = axios.get(`${BASE_URL}/api/vi-tri`, {
    headers: configHeaders(),
  });

  return response;
};

export const getProvince = () => {
  const response = axios.get(
    `${BASE_URL}/api/vi-tri/phan-trang-tim-kiem?pageIndex=1&pageSize=8`,
    {
      headers: configHeaders(),
    }
  );

  return response;
};

export const getRoomByLocation = (maViTri) => {
  const response = axios.get(
    `${BASE_URL}/api/phong-thue/lay-phong-theo-vi-tri?maViTri=${maViTri}`,
    {
      headers: configHeaders(),
    }
  );

  return response;
};
