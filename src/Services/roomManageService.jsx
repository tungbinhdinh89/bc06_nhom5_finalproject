import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
import { localAdminService } from "./localServ";

export const roomManageService = {
  getRoom: () => {
    return axios({
      url: `${BASE_URL}/api/phong-thue`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  addRoom: (roomAdd) => {
    const token = localAdminService.get()?.token;
    console.log(token);
    return axios({
      url: `${BASE_URL}/api/phong-thue`,
      method: "POST",
      data: roomAdd,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  deleteRoom: (id) => {
    const token = localAdminService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/phong-thue/${id}`,
      method: "DELETE",
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  getRoomById: (id) => {
    return axios({
      url: `${BASE_URL}/api/phong-thue/${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  putRoomById: (id, roomDataById) => {
    const token = localAdminService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/phong-thue/${id}`,
      method: "PUT",
      data: roomDataById,
      headers: {
        ...configHeaders(),
        token: token,
        "Content-Type": "multipart/form-data",
      },
    });
  },
  postImageRoom: (roomFile) => {
    const token = localAdminService.get()?.token;
    const formData = new FormData();
    formData.append("formFile", roomFile);

    return axios({
      url: `${BASE_URL}/api/users/upload-hinh-phong`,
      method: "POST",
      data: formData,
      headers: {
        ...configHeaders(),
        token: token,
        "Content-Type": "multipart/form-data",
      },
    });
  },
};
