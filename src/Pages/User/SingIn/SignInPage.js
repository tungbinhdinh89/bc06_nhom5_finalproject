import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { message } from "antd";
import { Button, Checkbox, Form, Input } from "antd";
import { setUserLoginAction } from "../../../redux/action/userLoginAction";

function SignInPage() {
  let navigate = useNavigate();

  const refreshPage = () => {
    navigate(0);
  };
  let dispatch = useDispatch();

  // form antd
  const onFinish = (values) => {
    let onSuccess = () => {
      message.success("Đăng nhập thành công");
      navigate("/");
      refreshPage();
    };
    let onError = () => {
      message.error("Đăng nhập thất bại");
    };
    dispatch(setUserLoginAction(values, onSuccess, onError));
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div
      className="w-screen h-screen relative mt-[100px]"
      style={{
        backgroundImage:
          'url("http://demo4.cybersoft.edu.vn/static/media/logo_login.a444f2681cc7b623ead2.jpg")',
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}>
      <div className="w-full p-5">
        <div className="md:absolute top-1/2 left-1/2 md:-translate-x-1/2 md:-translate-y-1/2 border rounded-lg shadow-lg bg-white px-10 py-5 w-4/5 sm:w-2/3 mx-auto">
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off">
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  type: "email",
                  message: "Email không hợp lệ!",
                },
                {
                  required: true,
                  message: "Vui lòng điền Email của bạn!",
                },
              ]}>
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}>
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{ offset: 8, span: 16 }}>
              <Checkbox>Remember me</Checkbox>
              <p>
                {" "}
                Chưa có tài khoản?{" "}
                <Link to="/sign-up">
                  <span>Đăng Ký</span>
                </Link>
              </p>
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" className="bg-red-600">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
export default SignInPage;
