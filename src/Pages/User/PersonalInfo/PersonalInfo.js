import React from "react";
import { useSelector } from "react-redux";
import EditInfo from "./components/EditInfo";
import RoomByUser from "./components/RoomByUser";
import VerifyInfo from "./components/VerifyInfo";
import Box from "@mui/material/Box";

import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
};

function PersonalInfo() {
  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);

  const userLoginInfo = userSingIn?.user;

  const maNguoiDung = userLoginInfo?.id || 0;

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div className="flex pt-10 container justify-center ">
      <div className=" w-1/3 flex justify-normal items-start">
        <VerifyInfo userLoginInfo={userLoginInfo} />
      </div>
      <div className="w-full ">
        <div className=" flex flex-col justify-center items-start px-40 ">
          <h1 className="text-2xl font-bold pb-2 font-sans">
            Xin Chào, tôi là {userLoginInfo?.name}
          </h1>
          <p className="text-md text-gray-500  font-semibold pb-2">
            Bắt đầu tham gia vào 2021
          </p>
          <button
            className="font-bold cursor-pointer underline text-[16px] pb-2"
            onClick={handleOpen}>
            Chỉnh sửa hồ sơ
          </button>
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description">
            <Box sx={style}>
              <EditInfo userLoginInfo={userLoginInfo} setOpen={setOpen} />
            </Box>
          </Modal>
        </div>

        <div className="ml-[100px] "></div>

        <div className="pl-20 ">
          <h1 className="text-2xl font-extrabold pb-2">Phòng Cho Thuê</h1>
          <RoomByUser maNguoiDung={maNguoiDung} />
        </div>
      </div>
    </div>
  );
}

export default PersonalInfo;
