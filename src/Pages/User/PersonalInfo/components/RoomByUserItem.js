import React, { useEffect, useState } from "react";
import { getRoomDetail } from "../../../../Services/roomServ";

function RoomByUserItem({ roomsByUser }) {
  const maPhong = roomsByUser.maPhong;

  const [roomOrderByUser, setRoomOrderByUser] = useState([]);

  useEffect(() => {
    getRoomDetail(maPhong)
      .then((res) => {
        setRoomOrderByUser(res.data.content);
      })
      .catch((err) => {});
  }, [maPhong]);
  return (
    <div className="border-b-2 pb-3 w-full relative pt-3 z-0">
      <div className="overflow-hidden flex">
        <div className="">
          <img
            src={roomOrderByUser.hinhAnh}
            alt={roomOrderByUser.tenPhong}
            style={{ height: 200, width: 300 }}
            className=" rounded-[1rem] h-[200px] w-[280px] object-cover"
          />
          <button className="absolute top-3 right-3 z-30">
            <svg
              viewBox="0 0 32 32"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="presentation"
              focusable="false"
              style={{
                display: "block",
                fill: "rgba(0, 0, 0, 0.5)",
                height: 24,
                width: 24,
                stroke: "rgb(255, 255, 255)",
                strokeWidth: 2,
                overflow: "hidden",
              }}>
              <path d="m16 28c7-4.733 14-10 14-17 0-1.792-.683-3.583-2.05-4.95-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05l-2.051 2.051-2.05-2.051c-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05-1.367 1.367-2.051 3.158-2.051 4.95 0 7 7 12.267 14 17z" />
            </svg>
          </button>
        </div>
        <div className="ml-4">
          <p className="font-bold  text-ellipsis overflow-hidden whitespace-nowrap mt-2">
            {roomOrderByUser.tenPhong}
          </p>
          <p className="text-gray-500 text-ellipsis overflow-hidden whitespace-nowrap">
            Phòng bao gồm:
          </p>

          <p className="text-gray-500">
            <span>{roomOrderByUser.khach} khách, </span>
            <span>{roomOrderByUser.giuong} giường, </span>
            <span>{roomOrderByUser.phongTam} phòng tắm, </span>
            <span>{roomOrderByUser.phongNgu} phòng ngủ, </span>
            <span>{roomOrderByUser.banLa && "Bàn là, "}</span>
            <span>{roomOrderByUser.banUi && "Bàn ủi, "}</span>
            <span>{roomOrderByUser.bep && "Bếp, "}</span>
            <span>{roomOrderByUser.dieuHoa && "Điều hoà, "}</span>
            <span>{roomOrderByUser.doXe && "Chỗ đỗ xe, "}</span>
            <span>{roomOrderByUser.hoBoi && "Hồ bơi, "}</span>
            <span>{roomOrderByUser.mayGiat && "Máy giặt, "}</span>
            <span>{roomOrderByUser.tivi && "Tivi, "}</span>
            <span>{roomOrderByUser.wifi && "Wifi. "}</span>
          </p>
          <p className="mt-1 flex justify-end">
            <span className="font-bold">{roomOrderByUser.giaTien}</span> /đêm
          </p>
        </div>
      </div>
    </div>
  );
}

export default RoomByUserItem;
