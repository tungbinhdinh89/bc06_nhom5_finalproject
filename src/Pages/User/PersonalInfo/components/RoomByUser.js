import React, { useEffect, useState } from "react";
import { getOderRoomByUser } from "../../../../Services/userServ";
import RoomByUserItem from "./RoomByUserItem";

function RoomByUser({ maNguoiDung }) {
  const [orderRoomByUser, setOrderRoomByUser] = useState([]);

  useEffect(() => {
    getOderRoomByUser(maNguoiDung)
      .then((res) => setOrderRoomByUser(res.data.content))
      .catch((err) => {});
  }, [maNguoiDung]);

  return (
    <div className="z-1">
      {orderRoomByUser.map((room) => {
        return <RoomByUserItem key={room.id} roomsByUser={room} />;
      })}
    </div>
  );
}

export default RoomByUser;
