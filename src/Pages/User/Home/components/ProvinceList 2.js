import React, { useEffect, useState } from "react";
import { getProvince } from "../../../../services/locationServ";
import { Typography } from "antd";
import { Link } from "react-router-dom";

function ProvinceList() {
  const [province, setProvince] = useState([]);
  useEffect(() => {
    getProvince()
      .then((res) => {
        setProvince(res.data.content.data);
      })
      .catch((err) => {});
  }, []);

  return (
    <div className="container mx-auto flex flex-col justify-center items-center text-center mt-20">
      <Typography.Title className="pb-10">
        Khám phá những điểm đến gần đây
      </Typography.Title>
      <div className=" grid sm:grid-cols-2 xl:grid-cols-4 md:gap-20">
        {province.map((item, index) => {
          return (
            <Link to="#" className="" key={index}>
              <div className="flex relative  mr-5 hover:translate-y-[-10px] transition">
                <img
                  src={item.hinhAnh}
                  alt="anh"
                  style={{
                    height: 300,
                    width: 300,
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                  className="rounded-2xl"
                />
                <div className="ml-2 absolute top-5 left-5">
                  <Typography.Text
                    strong
                    className="text-xl text-white shadow-sm">
                    {item.tinhThanh}
                  </Typography.Text>
                  <Typography.Paragraph className="text-white font-bold shadow-md">
                    15 phút lái xe
                  </Typography.Paragraph>
                </div>
              </div>
            </Link>
          );
        })}
      </div>
    </div>
  );
}

export default ProvinceList;
