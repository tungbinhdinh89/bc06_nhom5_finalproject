import React from "react";
import Review from "./components/Review";
import RoomDetail from "./components/RoomDetail";
import { useParams } from "react-router";
import Comments from "./components/Comments";
import { useSelector } from "react-redux";

function DetailRoomPage() {
  const params = useParams();
  // const userLoginInfo = useSelector((state) => state.postSignIn.userSingIn);
  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);

  const userLoginInfo = userSingIn?.user;
  const avatar = userLoginInfo?.avatar;
  const token = userSingIn?.token;

  return (
    <div className="container mx-auto ">
      <RoomDetail maPhong={params.id} />
      <Review maPhong={params.id} />
      {userLoginInfo && <Comments avatar={avatar} token={token} />}
    </div>
  );
}

export default DetailRoomPage;
