import React, { useEffect, useState } from "react";
import { getRoomDetail } from "../../../../Services/roomServ";
import AcUnitOutlinedIcon from "@mui/icons-material/AcUnitOutlined";
import PoolOutlinedIcon from "@mui/icons-material/PoolOutlined";
import IronIcon from "@mui/icons-material/Iron";
import TvIcon from "@mui/icons-material/Tv";
import WifiIcon from "@mui/icons-material/Wifi";
import CountertopsIcon from "@mui/icons-material/Countertops";
import LocalParkingOutlinedIcon from "@mui/icons-material/LocalParkingOutlined";
import ElevatorOutlinedIcon from "@mui/icons-material/ElevatorOutlined";
import LocalLaundryServiceOutlinedIcon from "@mui/icons-material/LocalLaundryServiceOutlined";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import Booking from "./Booking";

function RoomDetail({ maPhong }) {
  const [roomDetail, setRoomDetail] = useState([]);

  useEffect(() => {
    getRoomDetail(maPhong)
      .then((res) => {
        setRoomDetail(res.data.content);
      })
      .catch((err) => {});
  }, [maPhong]);
  return (
    <>
      <div className="">
        <p>
          <span className="font-semibold text-xl sm:text-3xl tracking-widest leading-relaxed text-gray-900">
            {roomDetail.tenPhong}
          </span>
        </p>
        <div className="flex flex-wrap justify-between items-center">
          <div>
            <span className="text-sm font-normal tracking-widest">
              <i className="fa fa-star" /> 5 sao .
            </span>{" "}
            <span className="underline text-sm font-normal tracking-widest mx-1">
              52 đánh giá
            </span>
            .
            <span className="text-sm font-normal tracking-widest mx-1">
              {" "}
              <i className="fa-solid fa-award" /> Chủ nhà siêu cấp .
            </span>
            <span className="underline text-sm font-normal tracking-widest mx-1">
              Đình Sơn City, Hồ Chí Minh, Việt Nam
            </span>
          </div>
          <div className="flex flex-wrap justify-center items-center">
            <button className="px-2 py-1 hover:bg-gray-100 rounded-md transition-all duration-150 flex justify-center items-center font-semibold text-sm text-gray-700">
              <svg
                viewBox="0 0 32 32"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="presentation"
                focusable="false"
                style={{
                  display: "inline-block",
                  fill: "none",
                  height: 16,
                  width: 16,
                  stroke: "currentcolor",
                  strokeWidth: 2,
                  overflow: "visible",
                }}>
                <g fill="none">
                  <path d="M27 18v9a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-9" />
                  <path d="M16 3v23V3z" />
                  <path d="M6 13l9.293-9.293a1 1 0 0 1 1.414 0L26 13" />
                </g>
              </svg>
              <span className="ml-2">Chia sẻ</span>
            </button>
            <button className="px-2 py-1 hover:bg-gray-100 rounded-md transition-all duration-150  flex justify-center items-center font-semibold text-sm text-gray-700">
              <svg
                viewBox="0 0 32 32"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
                role="presentation"
                focusable="false"
                style={{
                  display: "inline-block",
                  fill: "none",
                  height: 16,
                  width: 16,
                  stroke: "currentcolor",
                  strokeWidth: 2,
                  overflow: "visible",
                }}>
                <path d="m16 28c7-4.733 14-10 14-17 0-1.792-.683-3.583-2.05-4.95-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05l-2.051 2.051-2.05-2.051c-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05-1.367 1.367-2.051 3.158-2.051 4.95 0 7 7 12.267 14 17z" />
              </svg>
              <span className="ml-2">Lưu</span>
            </button>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-2 mt-5">
        <div className="rounded-l-xl rounded-r-xl sm:rounded-r-none overflow-hidden">
          <img
            className="w-full object-contain rounded-l-xl overflow-hidden"
            alt={roomDetail.tenPhong}
            src={roomDetail.hinhAnh}
            style={{ imageRendering: "pixelated" }}
          />
        </div>
        <div className="hidden sm:grid sm:grid-cols-2 gap-2 ">
          <div>
            <img
              className="w-full h-full"
              alt={roomDetail.tenPhong}
              src={roomDetail.hinhAnh}
            />
          </div>
          <div className="hidden md:block rounded-tr-xl overflow-hidden">
            <img
              className="w-full h-full rounded-tr-xl overflow-hidden"
              alt={roomDetail.tenPhong}
              src={roomDetail.hinhAnh}
            />
          </div>
          <div className="rounded-r-xl overflow-hidden md:rounded-none">
            <img
              className="w-full h-full"
              alt={roomDetail.tenPhong}
              src={roomDetail.hinhAnh}
            />
          </div>
          <div className="hidden md:block rounded-br-xl overflow-hidden">
            <img
              className="w-full h-full rounded-br-xl overflow-hidden"
              alt={roomDetail.tenPhong}
              src={roomDetail.hinhAnh}
            />
          </div>
        </div>
      </div>
      <div className="w-full flex sm:flex-row flex-col mt-10 border-b pb-5">
        <div className="w-full sm:w-1/2 lg:w-3/5">
          <div className="flex flex-wrap justify-between items-center pb-5 border-b">
            <div>
              <h1 className="font-semibold text-lg sm:text-2xl text-gray-800">
                Toàn bộ căn hộ. Chủ nhà Sungwon
              </h1>
              <span className="text-sm font-normal  tracking-widest text-gray-700">
                <span>{roomDetail.khach} khách . </span>
                <span className=" mx-1">
                  {roomDetail.phongNgu} phòng ngủ .{" "}
                </span>
                <span className=" mx-1">
                  {roomDetail.phongTam} phòng tắm .{" "}
                </span>
              </span>
            </div>
            <div className="w-12 h-12  relative">
              <img
                src="https://airbnb.cybersoft.edu.vn/public/temp/1663926315959_halong-bay-vietnam-from-above-gettyimages.jpg"
                alt="hinhAnh"
                className="w-full h-full rounded-full overflow-hidden"
              />
              <div className="absolute bottom-0 -right-1 text-2xl">
                <svg
                  viewBox="0 0 14 24"
                  role="presentation"
                  aria-hidden="true"
                  focusable="false"
                  style={{
                    height: "1em",
                    width: "1em",
                    display: "block",
                    fill: "currentcolor",
                  }}>
                  <path
                    d="m10.5 20.0005065c0 1.9326761-1.56704361 3.4994935-3.5 3.4994935s-3.5-1.5668174-3.5-3.4994935c0-1.9326762 1.5670426-3.5005065 3.5-3.5005065s3.5 1.5678303 3.5 3.5005065m-9.99486248-18.58757644-.00513752 8.13836018c0 .45796416.21682079.88992936.58880718 1.17090736l5.07730539 3.831699c.4870761.367971 1.16836618.367971 1.65647028.0009994l5.08141685-3.8266984c.3719859-.2789784.5898342-.7109444.5908612-1.16790827.0010271-1.75186288.0041101-6.21051146.0051391-8.14035983 0-.50396002-.4202834-.91292822-.9392158-.91292822l-11.11643181-.00699945c-.51790391-.00099942-.93818728.40796878-.93921487.91292823"
                    fill="#fff"
                  />
                  <path
                    d="m12 9.5-5-3.70124468 5-3.79875532zm-6.1292309 9.187485c-.52182677.3180834-.8707691.8762459-.8707691 1.5144379 0 .9937534.83703449 1.7980771 1.870162 1.7980771.81806646 0 1.50434636-.5065007 1.75946763-1.2095239z"
                    fill="#ffb400"
                  />
                  <path d="m12 9.5-5 3.75-5-3.75v-7.5z" fill="#ff5a5f" />
                  <path
                    d="m7 24c-2.2060547 0-4-1.7939453-4-3.9990234 0-2.2060547 1.7939453-4.0009766 4-4.0009766s4 1.7949219 4 4.0009766c0 2.2050781-1.7939453 3.9990234-4 3.9990234zm0-7c-1.6542969 0-3 1.3466797-3 3.0009766 0 1.6533203 1.3457031 2.9990234 3 2.9990234s3-1.3457031 3-2.9990234c0-1.6542969-1.3457031-3.0009766-3-3.0009766zm.0039062-1.8242188c-.4560547 0-.9121094-.1064453-1.2617188-.3164062l-5.0458984-3.8642578c-.4697265-.3642578-.696289-.8525391-.696289-1.4951172v-8c.0009766-.3730469.1679688-.7529297.4580078-1.0429688.2900391-.2905273.6689453-.4570312 1.0410156-.4570312h.0019531 10.9990235c.7851562 0 1.5.7148438 1.5 1.5v7.9277344c-.0009766.6762695-.2421875 1.2177734-.6953125 1.5668945l-5.0009766 3.8325195c-.3505859.2333985-.8251953.3486328-1.2998047.3486328zm-5.5058593-14.1757812c-.1044922 0-.2324219.0625-.3330078.1635742-.1015625.1020508-.1650391.230957-.1650391.3374024v7.9990234c0 .3305664.0888672.5341797.3066406.703125l4.9970703 3.8310547c.3330078.1953125 1.0859375.2001953 1.4208984-.0205078l4.9716797-3.8125c.2001954-.1542969.3027344-.4155274.303711-.7749024v-7.9267578c0-.2285156-.2714844-.4995117-.5-.4995117h-11-.0009766s0 0-.0009765 0z"
                    fill="#484848"
                  />
                </svg>
              </div>
            </div>
          </div>
          <div className="mt-5 pb-5 border-b">
            <div className="flex w-full">
              <div className="pt-2">
                <svg
                  viewBox="0 0 32 32"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
                  role="presentation"
                  focusable="false"
                  style={{
                    display: "inline-block",
                    height: 24,
                    width: 24,
                    fill: "currentcolor",
                  }}>
                  <path d="m16 17c3.8659932 0 7 3.1340068 7 7s-3.1340068 7-7 7-7-3.1340068-7-7 3.1340068-7 7-7zm0 2c-2.7614237 0-5 2.2385763-5 5s2.2385763 5 5 5 5-2.2385763 5-5-2.2385763-5-5-5zm9.6666667-18.66666667c1.0543618 0 1.9181651.81587779 1.9945142 1.85073766l.0054858.14926234v6.38196601c0 .70343383-.3690449 1.35080636-.9642646 1.71094856l-.1413082.0779058-9.6666667 4.8333334c-.5067495.2533747-1.0942474.2787122-1.6171466.0760124l-.1717078-.0760124-9.66666666-4.8333334c-.62917034-.3145851-1.04315599-.93418273-1.09908674-1.62762387l-.00648607-.16123049v-6.38196601c0-1.05436179.81587779-1.91816512 1.85073766-1.99451426l.14926234-.00548574zm0 2h-19.33333337v6.38196601l9.66666667 4.83333336 9.6666667-4.83333336z" />
                </svg>
              </div>
              <div className="ml-4">
                <h3 className="font-semibold text-gray-800 text-base sm:text-lg ">
                  Sungwon là Chủ nhà siêu cấp
                </h3>
                <p className="tracking-wider text-gray-500">
                  {roomDetail.moTa}
                </p>
              </div>
            </div>
            <div className="flex mt-5">
              <div className="pt-2">
                <span>
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "inline-block",
                      height: 24,
                      width: 24,
                      fill: "currentcolor",
                    }}>
                    <path d="M16 0c6.627 0 12 5.373 12 12 0 6.337-3.814 12.751-11.346 19.257L16 31.82l-1.076-.932C7.671 24.509 4 18.218 4 12 4 5.423 9.397 0 16 0zm0 2C10.504 2 6 6.525 6 12c0 5.44 3.249 11.118 9.831 17.02l.169.149.576-.518c6.178-5.65 9.293-11.092 9.42-16.318L26 12c0-5.523-4.477-10-10-10zm0 5a5 5 0 1 1 0 10 5 5 0 0 1 0-10zm0 2a3 3 0 1 0 0 6 3 3 0 0 0 0-6z" />
                  </svg>
                </span>
              </div>
              <div className="ml-4">
                <h3 className="font-semibold text-gray-800 text-base sm:text-lg ">
                  Địa điểm tuyệt vời
                </h3>
                <p className="tracking-wider text-gray-500">
                  90% khách gần đây đã xếp hạng 5 sao cho vị trí này.
                </p>
              </div>
            </div>
            <div className="flex mt-5">
              <div className="pt-2">
                <span>
                  <svg
                    viewBox="0 0 32 32"
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    role="presentation"
                    focusable="false"
                    style={{
                      display: "inline-block",
                      height: 24,
                      width: 24,
                      fill: "currentcolor",
                    }}>
                    <path d="m11.6667 0-.00095 1.666h8.667l.00055-1.666h2l-.00055 1.666 6.00065.00063c1.0543745 0 1.9181663.81587127 1.9945143 1.85073677l.0054857.14926323v15.91907c0 .4715696-.1664445.9258658-.4669028 1.2844692l-.1188904.1298308-8.7476886 8.7476953c-.3334303.3332526-.7723097.5367561-1.2381975.5778649l-.1758207.0077398h-12.91915c-2.68874373 0-4.88181754-2.1223321-4.99538046-4.7831124l-.00461954-.2168876v-21.66668c0-1.05436021.81587582-1.91815587 1.85073739-1.99450431l.14926261-.00548569 5.999-.00063.00095-1.666zm16.66605 11.666h-24.666v13.6673c0 1.5976581 1.24893332 2.9036593 2.82372864 2.9949072l.17627136.0050928 10.999-.0003.00095-5.6664c0-2.6887355 2.122362-4.8818171 4.7832071-4.9953804l.2168929-.0046196 5.66595-.0006zm-.081 8-5.58495.0006c-1.5977285 0-2.9037573 1.2489454-2.9950071 2.8237299l-.0050929.1762701-.00095 5.5864zm-18.586-16-5.999.00062v5.99938h24.666l.00065-5.99938-6.00065-.00062.00055 1.66733h-2l-.00055-1.66733h-8.667l.00095 1.66733h-2z" />
                  </svg>
                </span>
              </div>
              <h3 className="ml-4 font-semibold text-gray-800  text-base sm:text-lg">
                Miễn phí hủy trong 48 giờ.
              </h3>
            </div>
          </div>
          <div className="mt-5 pb-5 border-b">
            <h2>
              <img
                src="https://a0.muscache.com/im/pictures/54e427bb-9cb7-4a81-94cf-78f19156faad.jpg"
                alt="hinhAnh"
                className="h-7 mb-4"
              />
            </h2>
            <p className="text-base tracking-wider text-gray-800 mb-2">
              Mọi đặt phòng đều được bảo vệ miễn phí trong trường hợp Chủ nhà
              hủy, thông tin nhà/phòng cho thuê không chính xác và những vấn đề
              khác như sự cố trong quá trình nhận phòng.
            </p>
            <button className="font-semibold underline text-base text-gray-800  tracking-wider">
              Tìm hiểu thêm
            </button>
          </div>
          <div className="mt-5 pb-5 border-b">
            <div className="flex mb-4">
              <div className="mr-2">
                <svg
                  viewBox="0 0 16 16"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
                  role="presentation"
                  focusable="false"
                  style={{
                    display: "block",
                    height: 16,
                    width: 16,
                    fill: "currentcolor",
                  }}>
                  <path d="M9 0a1 1 0 0 1 .993.883L10 1v5h5a1 1 0 0 1 .993.883L16 7v8a1 1 0 0 1-.883.993L15 16H7a1 1 0 0 1-.993-.883L6 15v-5H1a1 1 0 0 1-.993-.883L0 9V1A1 1 0 0 1 .883.007L1 0h8zm1.729 7l-1.393.495.233.217.13.132c.125.127.227.245.308.352l.073.103.048.073.045.077H7.308v1.309h1.207l.166.52.09.266.112.29a6.294 6.294 0 0 0 1.109 1.789c-.495.315-1.119.607-1.87.87l-.331.112-.346.108-.445.134L7.72 15l.407-.125.386-.128c1.007-.349 1.836-.752 2.486-1.214.57.405 1.277.764 2.12 1.08l.369.134.386.128.406.125.72-1.153-.445-.134-.26-.08-.345-.115c-.783-.27-1.43-.57-1.94-.895a6.3 6.3 0 0 0 1.068-1.694l.128-.32.114-.33.165-.521h1.208V8.449H11.64l-.093-.231a3.696 3.696 0 0 0-.554-.917l-.126-.149-.14-.152zm1.35 2.758l-.042.133-.076.224-.103.264A4.985 4.985 0 0 1 11 11.76a4.952 4.952 0 0 1-.743-1.127l-.115-.254-.103-.264-.076-.224-.042-.133h2.158zM9 1H1v8h5V7c0-.057.005-.113.014-.167H3.827L3.425 8H2l2.257-6h1.486l1.504 4H9V1zM5 3.411L4.253 5.6h1.502L5 3.411z" />
                </svg>
              </div>
            </div>
            <p className="text-base tracking-wider text-gray-800 mb-4">
              Nhà nghỉ thôn dã hình lưỡi liềm trong một ngôi làng nghệ thuật gốm
              hai nghìn năm. Một ngôi nhà nguyên khối lớn với sân thượng ba tầng
              của Bảo tàng Văn hóa Guitar Serra, nổi tiếng với mặt tiền đặc sắc
              trong một ngôi làng nghệ thuật gốm hai nghìn năm pha trộn rất tốt
              với thiên nhiên.
            </p>
            <p className="text-base tracking-wider text-gray-800 mb-4">
              Tận hưởng kỳ nghỉ dưỡng sức cảm xúc thư giãn trong một căn phòng
              ấm cúng, chào...
            </p>
            <button className="underline font-semibold text-base tracking-wider text-gray-800">
              Hiển thị thêm
              <span className="ml-1">
                <svg
                  viewBox="0 0 18 18"
                  role="presentation"
                  aria-hidden="true"
                  focusable="false"
                  style={{
                    height: 12,
                    width: 12,
                    display: "inline-block",
                    fill: "var(--f-k-smk-x)",
                  }}>
                  <path
                    d="m4.29 1.71a1 1 0 1 1 1.42-1.41l8 8a1 1 0 0 1 0 1.41l-8 8a1 1 0 1 1 -1.42-1.41l7.29-7.29z"
                    fillRule="evenodd"
                  />
                </svg>
              </span>
            </button>
          </div>
          <div className="mt-5 pb-5">
            <div>
              <h2 className="font-semibold text-gray-800 text-xl pb-4">
                Nơi này có những gì cho bạn
              </h2>
            </div>
            <div className="grid grid-cols-2">
              {roomDetail.mayGiat && (
                <div className="flex items-center pb-4">
                  <LocalLaundryServiceOutlinedIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Máy giặt
                  </div>
                </div>
              )}
              {roomDetail.banLa && (
                <div className="flex items-center pb-4">
                  <IronIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Bàn Là
                  </div>
                </div>
              )}
              {roomDetail.tivi && (
                <div className="flex items-center pb-4">
                  <TvIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Tivi
                  </div>
                </div>
              )}
              {roomDetail.dieuHoa && (
                <div className="flex items-center pb-4">
                  <AcUnitOutlinedIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Máy điều hoà
                  </div>
                </div>
              )}
              {roomDetail.wifi && (
                <div className="flex items-center pb-4">
                  <WifiIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Wifi
                  </div>
                </div>
              )}
              {roomDetail.bep && (
                <div className="flex items-center pb-4">
                  <CountertopsIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Bếp
                  </div>
                </div>
              )}
              {roomDetail.doXe && (
                <div className="flex items-center pb-4">
                  <LocalParkingOutlinedIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Chỗ đỗ xe
                  </div>
                </div>
              )}
              {roomDetail.hoBoi && (
                <div className="flex items-center pb-4">
                  <PoolOutlinedIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Hồ bơi
                  </div>
                </div>
              )}
              {roomDetail.banUi && (
                <div className="flex items-center pb-4">
                  <ElevatorOutlinedIcon />
                  <div className="ml-4 text-base tracking-wider text-gray-800">
                    Thang máy
                  </div>
                </div>
              )}
            </div>
            <div className="mt-5">
              <button className="border border-solid border-gray-900 hover:bg-gray-100 transition-all duration-200 rounded-md px-5 py-3 font-semibold text-base text-gray-800 tracking-wider">
                Hiển thị tất cả 75 tiện nghi
              </button>
            </div>
          </div>
        </div>
        <Booking roomDetail={roomDetail} />
      </div>
    </>
  );
}

export default RoomDetail;
