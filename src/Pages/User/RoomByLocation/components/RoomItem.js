import React from "react";
import { Link } from "react-router-dom";

function RoomItem({ room }) {
  return (
    <Link className="roomLink" to={`/detail-room/${room.id}`}>
      <div className="overflow-hidden">
        <div className="relative">
          <img
            src={room.hinhAnh}
            alt={room.tenPhong}
            className="block w-full rounded-[1rem] h-[280px] object-cover"
          />
          <button className="absolute top-3 right-3 z-30">
            <svg
              viewBox="0 0 32 32"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="presentation"
              focusable="false"
              style={{
                display: "block",
                fill: "rgba(0, 0, 0, 0.5)",
                height: 24,
                width: 24,
                stroke: "rgb(255, 255, 255)",
                strokeWidth: 2,
                overflow: "hidden",
              }}>
              <path d="m16 28c7-4.733 14-10 14-17 0-1.792-.683-3.583-2.05-4.95-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05l-2.051 2.051-2.05-2.051c-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05-1.367 1.367-2.051 3.158-2.051 4.95 0 7 7 12.267 14 17z" />
            </svg>
          </button>
        </div>
        <p className="flex justify-between mt-2">
          <span className="font-bold  text-ellipsis overflow-hidden whitespace-nowrap">
            {room.tenPhong}
          </span>
          <span>
            <i className="fa fa-star" /> 2.81
          </span>
        </p>
        <p className="text-gray-500 text-ellipsis overflow-hidden whitespace-nowrap">
          {room.moTa}
        </p>
        <p className="text-gray-500">Ngày 24 - Ngày 19 tháng 11</p>
        <p className="mt-1">
          <span className="font-bold">950000 VNĐ</span> đêm
        </p>
      </div>
    </Link>
  );
}

export default RoomItem;
