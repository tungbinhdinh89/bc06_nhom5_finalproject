import React from "react";

function RoomFilter() {
  return (
    <div>
      <div className="flex flex-wrap justify-between items-center pb-5">
        <span className="font-semibold">Hơn 1.000 chổ ở</span>
        <button
          type="button"
          className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 flex flex-wrap justify-center items-center">
          <svg
            viewBox="0 0 16 16"
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
            role="presentation"
            focusable="false"
            style={{
              display: "block",
              height: 16,
              width: 16,
              fill: "currentcolor",
            }}>
            <path d="M5 8c1.306 0 2.418.835 2.83 2H14v2H7.829A3.001 3.001 0 1 1 5 8zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm6-8a3 3 0 1 1-2.829 4H2V4h6.17A3.001 3.001 0 0 1 11 2zm0 2a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
          </svg>
          <span>Bộ lọc</span>
        </button>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-6 ml-4"></div>
    </div>
  );
}

export default RoomFilter;
