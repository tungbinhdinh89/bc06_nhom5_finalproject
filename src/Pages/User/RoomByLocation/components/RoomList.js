import React, { useEffect, useState } from "react";
import RoomItem from "./RoomItem";
import { getRoomByLocation } from "../../../../Services/locationServ";

function RoomList({ params }) {
  const [roomList, setRoomList] = useState();

  useEffect(() => {
    getRoomByLocation(params.id)
      .then((res) => {
        setRoomList(res.data.content);
      })
      .catch((err) => {});
  }, [params]);
  return (
    <>
      {roomList?.map((item, index) => {
        return (
          <div key={index}>
            <RoomItem room={item} />
          </div>
        );
      })}
    </>
  );
}

export default RoomList;
