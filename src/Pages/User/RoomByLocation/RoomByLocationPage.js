import React from "react";
import { useParams } from "react-router";
import Map from "../../../Components/Map";
import RoomFilter from "./components/RoomFilter";
import RoomList from "./components/RoomList";

function RoomByLocation() {
  const params = useParams();

  return (
    <div className=" mx-auto px-20 py-20">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
        <div className="">
          <RoomFilter />
          <div className="grid sm:grid-cols-1 md:grid-cols-2 gap-6">
            <RoomList params={params} />
          </div>
        </div>
        <div className="">
          <Map />
        </div>
      </div>
    </div>
  );
}

export default RoomByLocation;
