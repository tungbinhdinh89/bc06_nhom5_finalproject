import moment from "moment";

export const formatDate = (date, format = "YYYY-MM-DD") => {
  return moment(date).format(format);
};

export const formatDateFirst = (date, format = "DD/MM/YYYY") => {
  return moment(date).format(format);
};

export const toDate = (s) => {
  return moment(s).toDate();
};

export const sortDate = (arr) => {
  if (!arr || arr.length === 0) {
    return [];
  }
  const sorted = arr.sort((a, b) => moment(a).isBefore(b));

  return sorted;
};

export const fillDates = (min, max) => {
  let arr = [min];
  let curr = min;
  while (true) {
    curr = moment(curr).add(1, "d");

    if (moment(curr).isAfter(max)) {
      break;
    }

    arr.push(curr);
  }

  return arr;
};
